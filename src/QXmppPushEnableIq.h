// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#pragma once

#include <QXmppIq.h>

/**
 * @todo write docs
 */
class QXmppPushEnableIq : public QXmppIq
{
public:
    QString jid() const;
    void setJid(const QString &jid);
    
    QString node() const;
    void setNode(const QString &node);

protected:
    void parseElementFromChild(const QDomElement &element) override;
    void toXmlElementFromChild(QXmlStreamWriter *writer) const override;
    
private:
    QString m_node;
    QString m_jid;
};
