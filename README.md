<!--
SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>

SPDX-License-Identifier: CC0-1.0
-->

# QXPush

*QXmpp based XMPP Push Server*

