// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "QXPush.h"

QXPush::QXPush(QObject *parent)
    : QObject(parent)
{
}

QXPush::~QXPush() = default;
