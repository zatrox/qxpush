// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

// Qt
#include <QCoreApplication>
// QXPush
#include "QXPush.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    
    QXPush push;

    return app.exec();
}
