// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "QXmppPushEnableIq.h"

#include "QDomElement"

QString QXmppPushEnableIq::jid() const
{
    return m_jid;
}

void QXmppPushEnableIq::setJid(const QString &jid)
{
    m_jid = jid;
}

QString QXmppPushEnableIq::node() const
{
    return m_node;
}

void QXmppPushEnableIq::setNode(const QString &node)
{
    m_node = node;
}

void QXmppPushEnableIq::parseElementFromChild(const QDomElement &element)
{
    QDomElement enableElement = element.firstChildElement("enable");
    m_jid = enableElement.attribute("jid");
    m_node = enableElement.attribute("node");
    
}

void QXmppPushEnableIq::toXmlElementFromChild(QXmlStreamWriter *writer) const
{
    writer->writeStartElement("enable");
    writer->writeDefaultNamespace("urn:xmpp:push:0");
    writer->writeAttribute("jid", m_jid);
    writer->writeAttribute("node", m_node);
    writer->writeEndElement();
}
