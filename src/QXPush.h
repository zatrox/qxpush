// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#pragma once

#include <QObject>

class QXPush : public QObject
{
    Q_OBJECT

public:
    explicit QXPush(QObject *parent = nullptr);
    ~QXPush() override;

private:
};
